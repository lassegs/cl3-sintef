# [View the presentation here](https://lassegs.gitlab.io/cl3-sintef/)

Slides are in [slides/slides.md](https://gitlab.com/lassegs/cl3-sintef/-/blob/main/slides/slides.md).

Forked from the [Dubin template](https://gitlab.com/lassegs/dubintemplate), to provide HTML 5 framework for presentation. See that for docs.



