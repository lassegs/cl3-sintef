
<!-- .slide: data-background-color="black" data-background-video="https://anti-brands.fra1.digitaloceanspaces.com/brands/forskningsradet/media/NFR_INTROPOSTER_LONG_NAMETAG_2022-05-05-074407_pwrq.mp4" data-background-size="contain"-->

---

<!-- .slide: data-background="https://horizoneurope.gr/wp-content/uploads/2020/11/hub-horizon.png" data-background-size="10%" data-background-position="top 25px right 25px" -->



# HORIZON-CL3-2024

<small>June 27th - November 20th</small>

---

Link to presentation: https://lassegs.gitlab.io/cl3-sintef/

<div class="threes">
<div>


<svg class="r-stretch" width="13.92cm" height="13.92cm" viewBox="0 0 37 37" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
	<g id="QRcode">
		<rect x="0" y="0" width="37" height="37" fill="#ffffff"/>
		<path style="stroke:#000000" transform="translate(4,4.5)" d="M0,0h1M1,0h1M2,0h1M3,0h1M4,0h1M5,0h1M6,0h1M9,0h1M13,0h1M17,0h1M18,0h1M19,0h1M22,0h1M23,0h1M24,0h1M25,0h1M26,0h1M27,0h1M28,0h1M0,1h1M6,1h1M8,1h1M9,1h1M11,1h1M12,1h1M13,1h1M14,1h1M15,1h1M17,1h1M22,1h1M28,1h1M0,2h1M2,2h1M3,2h1M4,2h1M6,2h1M9,2h1M10,2h1M11,2h1M14,2h1M16,2h1M18,2h1M19,2h1M22,2h1M24,2h1M25,2h1M26,2h1M28,2h1M0,3h1M2,3h1M3,3h1M4,3h1M6,3h1M8,3h1M9,3h1M11,3h1M15,3h1M17,3h1M22,3h1M24,3h1M25,3h1M26,3h1M28,3h1M0,4h1M2,4h1M3,4h1M4,4h1M6,4h1M10,4h1M12,4h1M13,4h1M16,4h1M17,4h1M18,4h1M19,4h1M20,4h1M22,4h1M24,4h1M25,4h1M26,4h1M28,4h1M0,5h1M6,5h1M8,5h1M10,5h1M13,5h1M14,5h1M15,5h1M16,5h1M18,5h1M20,5h1M22,5h1M28,5h1M0,6h1M1,6h1M2,6h1M3,6h1M4,6h1M5,6h1M6,6h1M8,6h1M10,6h1M12,6h1M14,6h1M16,6h1M18,6h1M20,6h1M22,6h1M23,6h1M24,6h1M25,6h1M26,6h1M27,6h1M28,6h1M12,7h1M13,7h1M15,7h1M16,7h1M0,8h1M1,8h1M2,8h1M3,8h1M4,8h1M6,8h1M7,8h1M8,8h1M9,8h1M10,8h1M12,8h1M14,8h1M15,8h1M17,8h1M21,8h1M23,8h1M25,8h1M27,8h1M3,9h1M4,9h1M7,9h1M9,9h1M16,9h1M17,9h1M18,9h1M19,9h1M20,9h1M21,9h1M22,9h1M23,9h1M24,9h1M28,9h1M1,10h1M4,10h1M5,10h1M6,10h1M8,10h1M9,10h1M11,10h1M12,10h1M15,10h1M17,10h1M23,10h1M3,11h1M4,11h1M5,11h1M7,11h1M8,11h1M9,11h1M10,11h1M11,11h1M14,11h1M15,11h1M16,11h1M20,11h1M24,11h1M25,11h1M27,11h1M0,12h1M1,12h1M5,12h1M6,12h1M7,12h1M9,12h1M11,12h1M15,12h1M16,12h1M17,12h1M18,12h1M19,12h1M23,12h1M25,12h1M26,12h1M1,13h1M3,13h1M5,13h1M10,13h1M12,13h1M13,13h1M14,13h1M16,13h1M17,13h1M18,13h1M19,13h1M20,13h1M21,13h1M22,13h1M23,13h1M24,13h1M28,13h1M0,14h1M2,14h1M3,14h1M5,14h1M6,14h1M13,14h1M14,14h1M15,14h1M17,14h1M23,14h1M25,14h1M26,14h1M0,15h1M1,15h1M3,15h1M8,15h1M9,15h1M12,15h1M13,15h1M16,15h1M20,15h1M22,15h1M24,15h1M27,15h1M5,16h1M6,16h1M8,16h1M9,16h1M12,16h1M14,16h1M17,16h1M20,16h1M23,16h1M25,16h1M26,16h1M0,17h1M1,17h1M2,17h1M3,17h1M4,17h1M7,17h1M8,17h1M9,17h1M10,17h1M16,17h1M18,17h1M19,17h1M20,17h1M22,17h1M23,17h1M24,17h1M26,17h1M28,17h1M0,18h1M6,18h1M9,18h1M11,18h1M12,18h1M15,18h1M16,18h1M17,18h1M18,18h1M20,18h1M21,18h1M23,18h1M24,18h1M26,18h1M0,19h1M2,19h1M3,19h1M7,19h1M9,19h1M10,19h1M11,19h1M14,19h1M15,19h1M22,19h1M24,19h1M27,19h1M0,20h1M2,20h1M6,20h1M7,20h1M8,20h1M9,20h1M10,20h1M11,20h1M17,20h1M19,20h1M20,20h1M21,20h1M22,20h1M23,20h1M24,20h1M26,20h1M27,20h1M28,20h1M8,21h1M9,21h1M12,21h1M13,21h1M14,21h1M16,21h1M17,21h1M18,21h1M20,21h1M24,21h1M25,21h1M26,21h1M27,21h1M28,21h1M0,22h1M1,22h1M2,22h1M3,22h1M4,22h1M5,22h1M6,22h1M8,22h1M13,22h1M14,22h1M15,22h1M16,22h1M19,22h1M20,22h1M22,22h1M24,22h1M25,22h1M26,22h1M0,23h1M6,23h1M10,23h1M12,23h1M13,23h1M18,23h1M19,23h1M20,23h1M24,23h1M0,24h1M2,24h1M3,24h1M4,24h1M6,24h1M8,24h1M10,24h1M12,24h1M14,24h1M15,24h1M16,24h1M17,24h1M18,24h1M20,24h1M21,24h1M22,24h1M23,24h1M24,24h1M26,24h1M0,25h1M2,25h1M3,25h1M4,25h1M6,25h1M8,25h1M10,25h1M16,25h1M19,25h1M20,25h1M21,25h1M25,25h1M26,25h1M27,25h1M28,25h1M0,26h1M2,26h1M3,26h1M4,26h1M6,26h1M8,26h1M11,26h1M12,26h1M15,26h1M16,26h1M17,26h1M18,26h1M19,26h1M21,26h1M22,26h1M23,26h1M24,26h1M25,26h1M26,26h1M27,26h1M0,27h1M6,27h1M8,27h1M9,27h1M11,27h1M14,27h1M15,27h1M16,27h1M20,27h1M22,27h1M24,27h1M25,27h1M27,27h1M0,28h1M1,28h1M2,28h1M3,28h1M4,28h1M5,28h1M6,28h1M8,28h1M11,28h1M17,28h1M18,28h1M19,28h1M21,28h1M22,28h1M23,28h1M26,28h1"/>
	</g>
</svg>


</div>
<div>
<small>

![](https://i.imgur.com/yDOh95r.png)

Line Marie Sørsdal

`lms@rcn.no`

<code>[Democracy and Global Development](https://www.forskningsradet.no/en/Portfolios/democracy-global-development/)</code>
</small>
</div>

<div>
<small>

![](https://i.imgur.com/kdXfpYV.png)

Lasse Gullvåg Sætre

`lgs@rcn.no`

<code>[Enabling Technologies](https://www.forskningsradet.no/en/Portfolios/enabling-technologies/)</code>

</small>
</div>
</div>


---

<!-- .slide: data-background="https://horizoneurope.gr/wp-content/uploads/2020/11/hub-horizon.png" data-background-size="10%" data-background-position="top 25px right 25px" -->

1. [Fighting Crime and Terrorism (FCT)](https://home-affairs.ec.europa.eu/policies/internal-security/new-way-forward-internal-security_en)
2. [Effective Management of EU External Borders (BM)]()
3. Resilient Infrastructure (INFRA)
4. Increased Cybersecurity (CS)
5. Disaster-Resilient Society (DRS)
6. Support to Security Research and Innovation (SSRI)

---

![](https://i.imgur.com/hMeE2Vn.png)

---

## Policy developments

<div class="threes">

<div>

<small>

**Border Management**

- Update of EU maritime security strategy
- EU Arctic policy​
- Drone Strategy 2.0​
- EU Digital Identity (eID)​
- Legislative proposal on the digitalisation of travel documents and the facilitation of travel

</small>
​</div>

<div>
<small>

**Fighting Crime & Terrorism**

- EU Strategy to tackle Organised Crime 2021-2025 ​
- EU Strategy on Combatting Trafficking in Human Beings 2021-2025​
- EU strategy for a more effective fight against child sexual abuse (2020-2025)​
- Implementation of Terrorist Content Online Regulation (ongoing). Monitoring and evaluation in June 2023 and June 2024​
- Anti-corruption package
</small>
</div>

<div>
<small>

**Cybersecurity**

- AI Act​
- IRIS² (Infrastructure for Resilience, Interconnection & Security by Satellites)
- Review of the Network and Information Security (NIS2) Directive​
- EU Cyber Resilience Act​
​
</div>
</small>
</div>

--

## Policy developments (cont.)

**Resilient infrastructure**: 

- Critical Entities Resillience Directive (CER Directive)
- Council Recommendation on a Union-wide coordinated approach to strengthen the resilience of critical infrastructure.



---


## Fighting Crime and Terrorism (FCT)

#### Trends
- Cybercrime: Advanced cyber-attacks, need for forensic tools.
- Organized Crime: Dismantling transnational networks.

#### Debates
- Privacy vs. security in surveillance.
- AI ethics in crime prevention.

#### Initiatives
- Predictive policing models.
- International law enforcement cooperation.


--

#### [HORIZON-CL3-2024-FCT-01-01](https://ec.europa.eu/info/funding-tenders/opportunities/portal/screen/opportunities/topic-details/horizon-cl3-2024-fct-01-01)
**Mitigating new threats and adapting investigation strategies in the era of Internet of Things (IoT)**
- **Focus:** Develop tools and methods to combat IoT-related crime.
- **Outcomes:** Enhanced law enforcement capabilities, improved investigation techniques.

--

#### [HORIZON-CL3-2024-FCT-01-02](https://ec.europa.eu/info/funding-tenders/opportunities/portal/screen/opportunities/topic-details/horizon-cl3-2024-fct-01-02)
**Open Topic**
- **Focus:** Address emerging challenges in fighting crime and terrorism.
- **Outcomes:** Innovative law enforcement solutions, enhanced operational capabilities. Engage with Europol Innovation Lab.

--

#### [HORIZON-CL3-2024-FCT-01-03](https://ec.europa.eu/info/funding-tenders/opportunities/portal/screen/opportunities/topic-details/horizon-cl3-2024-fct-01-03)
**Lawful evidence collection in online child sexual abuse investigations, including undercover**
- **Focus:** Techniques for lawful evidence collection in online child abuse cases.
- **Outcomes:** Improved digital evidence collection, advanced investigative training.

--

#### [HORIZON-CL3-2024-FCT-01-04](https://ec.europa.eu/info/funding-tenders/opportunities/portal/screen/opportunities/topic-details/horizon-cl3-2024-fct-01-04)
**Radicalisation and gender**
- **Focus:** Understand and mitigate gender-specific aspects of radicalisation.
- **Outcomes:** Gender-sensitive prevention strategies, detection tools.

--

#### [HORIZON-CL3-2024-FCT-01-05](https://ec.europa.eu/info/funding-tenders/opportunities/portal/screen/opportunities/topic-details/horizon-cl3-2024-fct-01-05)
**Combating hate speech online and offline**
- **Focus:** Address and mitigate hate speech through innovative technologies.
- **Outcomes:** Enhanced detection tools, community resilience improvement.

--

#### [HORIZON-CL3-2024-FCT-01-06](https://ec.europa.eu/info/funding-tenders/opportunities/portal/screen/opportunities/topic-details/horizon-cl3-2024-fct-01-06)
**Open Topic**
- **Focus:** Address emerging criminal activities influenced by societal factors.
- **Outcomes:** New methodologies, improved law enforcement collaboration.

---

## Effective Management of EU External Borders (BM)

#### Trends
- Biometric border security.
- Integrated border management.

#### Debates
- Surveillance and human rights.
- Biometric data protection.

#### Initiatives
- Advanced maritime security.
- Automated border control.

--

#### [HORIZON-CL3-2024-BM-01-01](https://ec.europa.eu/info/funding-tenders/opportunities/portal/screen/opportunities/topic-details/horizon-cl3-2024-bm-01-01)
**Open Topic**
- **Focus:** Enhance security and efficiency of EU external borders.
- **Outcomes:** Innovative border management solutions, advanced surveillance technologies.

--

#### [HORIZON-CL3-2024-BM-01-02](https://ec.europa.eu/info/funding-tenders/opportunities/portal/screen/opportunities/topic-details/horizon-cl3-2024-bm-01-02)
**Interoperability for border and maritime surveillance and situational awareness**
- **Focus:** Improve system interoperability in border and maritime surveillance.
- **Outcomes:** Enhanced situational awareness, better coordination.

--

#### [HORIZON-CL3-2024-BM-01-03](https://ec.europa.eu/info/funding-tenders/opportunities/portal/screen/opportunities/topic-details/horizon-cl3-2024-bm-01-03)
**Advanced user-friendly, compatible, secure identity and travel document management**
- **Focus:** Develop secure and user-friendly identity and travel document systems.
- **Outcomes:** Improved document security, streamlined border processes.

--

#### [HORIZON-CL3-2024-BM-01-04](https://ec.europa.eu/info/funding-tenders/opportunities/portal/screen/opportunities/topic-details/horizon-cl3-2024-bm-01-04)
**Integrated risk-based border control that mitigates public security risk, reduces false positives and strengthens privacy**
- **Focus:** Implement risk-based approaches to border control.
- **Outcomes:** Enhanced security, reduced false positives, strengthened privacy.

--

#### [HORIZON-CL3-2024-BM-01-05](https://ec.europa.eu/info/funding-tenders/opportunities/portal/screen/opportunities/topic-details/horizon-cl3-2024-bm-01-05)
**Detection and tracking of illegal and trafficked goods**
- **Focus:** Technologies for detecting and tracking illicit goods.
- **Outcomes:** Improved smuggling and trafficking combat, enhanced tracking systems.

---

## Resilient Infrastructure (INFRA)


#### Trends
- Protecting critical infrastructures.
- Investment in smart cities.

#### Debates
- Public-private security partnerships.
- Upgrading legacy infrastructure.

#### Initiatives
- Resilient smart grids.
- Advanced monitoring systems.


--

#### [HORIZON-CL3-2024-INFRA-01-01](https://ec.europa.eu/info/funding-tenders/opportunities/portal/screen/opportunities/topic-details/horizon-cl3-2024-infra-01-01)
**Open Topic**
- **Focus:** Strengthen critical infrastructure resilience.
- **Outcomes:** Improved infrastructure preparedness and response, innovative resilience solutions.

--

#### [HORIZON-CL3-2024-INFRA-01-02](https://ec.europa.eu/info/funding-tenders/opportunities/portal/screen/opportunities/topic-details/horizon-cl3-2024-infra-01-02)
**Resilient and secure urban planning and new tools for EU territorial entities**
- **Focus:** Enhance urban planning for resilience and security.
- **Outcomes:** New urban resilience tools, better security planning.

--

#### [HORIZON-CL3-2024-INFRA-01-03](https://ec.europa.eu/info/funding-tenders/opportunities/portal/screen/opportunities/topic-details/horizon-cl3-2024-infra-01-03)
**Advanced real-time data analysis used for infrastructure resilience**
- **Focus:** Utilize real-time data analysis for infrastructure resilience.
- **Outcomes:** Enhanced real-time monitoring, improved infrastructure security.

---

## Increased Cybersecurity (CS)


#### Trends
- Rise in ransomware attacks.
- Quantum-resistant cryptography.

#### Debates
- Surveillance vs. privacy.
- Global cooperation in cybersecurity.

#### Initiatives
- Public-private collaboration.
- Strengthening cybersecurity workforce.

--

#### [HORIZON-CL3-2024-CS-01-01](https://ec.europa.eu/info/funding-tenders/opportunities/portal/screen/opportunities/topic-details/horizon-cl3-2024-cs-01-01)
**Approaches and tools for security in software and hardware development and assessment**
- **Focus:** Develop secure approaches for software and hardware.
- **Outcomes:** Improved security in products, enhanced assessment tools.

--

#### [HORIZON-CL3-2024-CS-01-02](https://ec.europa.eu/info/funding-tenders/opportunities/portal/screen/opportunities/topic-details/horizon-cl3-2024-cs-01-02)
**Post-quantum cryptography transition**
- **Focus:** Prepare for post-quantum cryptography.
- **Outcomes:** Resilient cryptographic solutions, transition strategies.

---

## Disaster-Resilient Society (DRS)

#### Trends
- AI and big data in disaster response.
- Focus on climate-related disasters.

#### Debates
- Drones and robotics ethics.
- Equity in disaster preparedness.

#### Initiatives
- Community resilience programs.
- Advanced rapid response technologies.


--

#### [HORIZON-CL3-2024-DRS-01-01](https://ec.europa.eu/info/funding-tenders/opportunities/portal/screen/opportunities/topic-details/horizon-cl3-2024-drs-01-01)
**Prevention, detection, response and mitigation of chemical, biological and radiological threats to agricultural production, feed and food processing, distribution and consumption**
- **Focus:** Address threats in agriculture and food sectors.
- **Outcomes:** Enhanced protection, improved threat response.

--

#### [HORIZON-CL3-2024-DRS-01-02](https://ec.europa.eu/info/funding-tenders/opportunities/portal/screen/opportunities/topic-details/horizon-cl3-2024-drs-01-02)
**Open Topic**
- **Focus:** Address various disaster resilience challenges.
- **Outcomes:** Innovative solutions, enhanced preparedness.

--

#### [HORIZON-CL3-2024-DRS-01-03](https://ec.europa.eu/info/funding-tenders/opportunities/portal/screen/opportunities/topic-details/horizon-cl3-2024-drs-01-03)
**Harmonised/Standard protocols for the implementation of alert and impact forecasting systems as well as transnational emergency management in the areas of high-impact weather/climatic and geological disasters**
- **Focus:** Develop harmonized disaster protocols.
- **Outcomes:** Improved forecasting and alerts, better emergency management.

--

#### [HORIZON-CL3-2024-DRS-01-04](https://ec.europa.eu/info/funding-tenders/opportunities/portal/screen/opportunities/topic-details/horizon-cl3-2024-drs-01-04)
**Hi-tech capacities for crisis response and recovery after a natural-technological (NaTech) disaster**
- **Focus:** Leverage high-tech for crisis response.
- **Outcomes:** Advanced response tools, improved crisis management.

--

#### [HORIZON-CL3-2024-DRS-01-05](https://ec.europa.eu/info/funding-tenders/opportunities/portal/screen/opportunities/topic-details/horizon-cl3-2024-drs-01-05)
**Cost-effective sustainable technologies and crisis management strategies for RN large-scale protection of population and infrastructures after a nuclear blast or nuclear facility incident**
- **Focus:** Develop sustainable technologies for nuclear incidents.
- **Outcomes:** Enhanced protection, sustainable crisis management solutions.

---

## Support to Security Research and Innovation (SSRI)

#### Trends
- Integrating advanced technologies in security.
- Enhancing EU security industry competitiveness.

#### Debates
- Balancing innovation and regulation.
- Ensuring ethical standards in security research.

#### Initiatives
- Fostering innovation through funding schemes.
- Promoting cross-sector collaboration.

--

#### [HORIZON-CL3-2024-SSRI-01-01](https://ec.europa.eu/info/funding-tenders/opportunities/portal/screen/opportunities/topic-details/horizon-cl3-2024-ssri-01-01)
**Demand-led innovation through public procurement**
- **Focus:** Encourage innovation through public procurement.
- **Outcomes:** Increased uptake of security solutions, enhanced public-innovator collaboration.

--

#### [HORIZON-CL3-2024-SSRI-01-02](https://ec.europa.eu/info/funding-tenders/opportunities/portal/screen/opportunities/topic-details/horizon-cl3-2024-ssri-01-02)
**Accelerating uptake through open proposals for advanced SME innovation**
- **Focus:** Support SMEs in security innovation.
- **Outcomes:** Strengthened SME capacity, improved market pathways.

---

<!-- .slide: data-background-video="https://anti-brands.fra1.digitaloceanspaces.com/brands/forskningsradet/media/NFR_ENDPOSTER-16-9_LIME_SHAPE.mp4" data-background-size="contain" data-background-opacity="1"-->